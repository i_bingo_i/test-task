Test tasks realisation
========================

**For start the application please execute next commands:**
1. composer start
    *  For working email sending you should enter data for your Gmail account now or after.
2. php bin/console doctrine:database:create
3. php bin/console doctrine:migrations:migrate
4. php bin/console doctrine:fixtures:load

Next you can verify me. Thank you for attention.
