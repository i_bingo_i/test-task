<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="world_markets")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WorldMarkets")
 * @ORM\HasLifecycleCallbacks()
 */
class WorldMarkets
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $companySymbol;


    /**
     * @ORM\Column(type="string")
     */
    private $companyName;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="float")
     */
    private $percent;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $open;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $high;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $low;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateCreate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateUpdate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return WorldMarkets
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return WorldMarkets
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set percent
     *
     * @param float $percent
     *
     * @return WorldMarkets
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     *
     * @return WorldMarkets
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }


    /**
     * @ORM\PrePersist()
     */
    public function updateModifiedDateCreate()
    {
        $this->setDateCreate(new \DateTime());
    }


    /**
     * Set open
     *
     * @param float $open
     *
     * @return WorldMarkets
     */
    public function setOpen($open)
    {
        $this->open = $open;

        return $this;
    }

    /**
     * Get open
     *
     * @return float
     */
    public function getOpen()
    {
        return $this->open;
    }

    /**
     * Set high
     *
     * @param float $high
     *
     * @return WorldMarkets
     */
    public function setHigh($high)
    {
        $this->high = $high;

        return $this;
    }

    /**
     * Get high
     *
     * @return float
     */
    public function getHigh()
    {
        return $this->high;
    }

    /**
     * Set low
     *
     * @param float $low
     *
     * @return WorldMarkets
     */
    public function setLow($low)
    {
        $this->low = $low;

        return $this;
    }

    /**
     * Get low
     *
     * @return float
     */
    public function getLow()
    {
        return $this->low;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Set companySymbol
     *
     * @param string $companySymbol
     *
     * @return WorldMarkets
     */
    public function setCompanySymbol($companySymbol)
    {
        $this->companySymbol = $companySymbol;

        return $this;
    }

    /**
     * Get companySymbol
     *
     * @return string
     */
    public function getCompanySymbol()
    {
        return $this->companySymbol;
    }

    /**
     * Set dateUpdate
     *
     * @param \DateTime $dateUpdate
     *
     * @return WorldMarkets
     */
    public function setDateUpdate($dateUpdate)
    {
        $this->dateUpdate = $dateUpdate;

        return $this;
    }

    /**
     * Get dateUpdate
     *
     * @return \DateTime
     */
    public function getDateUpdate()
    {
        return $this->dateUpdate;
    }

    /**
     * @ORM\PreUpdate()
     */
    public function updateModifiedDateUpdate()
    {
        $this->setDateUpdate(new \DateTime());
    }
}
