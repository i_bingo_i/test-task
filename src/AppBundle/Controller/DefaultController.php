<?php

namespace AppBundle\Controller;

use AppBundle\Form\LetterType;
use AppBundle\Services\CreateFileXML;
use AppBundle\Services\Creator;
use AppBundle\Services\DataStealer;
use AppBundle\Services\SendEmail;
use AppBundle\Services\WorldMarket as WorldMarketService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        /** @var WorldMarketService $worldMarketService */
        $worldMarketService = $this->get(WorldMarketService::class);
        /** @var array $worldMarketData */
        $worldMarketData = $worldMarketService->getDataForRender();
        /** @var CreateFileXML $creatorXML */
        $creatorXML = $this->get(CreateFileXML::class);
        /** @var Session $session */
        $session = $this->get('session');

        $message = $session->getFlashBag()->has('message') ? $session->getFlashBag()->get('message')[0] : '';

        $emailForm = $this->createForm(LetterType::class);

        return $this->render('default/my-index.html.twig', [
            'names' => $worldMarketData['names'],
            'percents' => $worldMarketData['percents'],
            'isExistFile' => $creatorXML->isExistFile(),
            'letterForm' => $emailForm->createView(),
            'message' => $message
        ]);
    }

    /**
     * @Route("/stealing", name="stealing_process", methods="POST")
     * @param Request $request
     * @return RedirectResponse
     */
    public function stealingAction(Request $request)
    {
        /** @var Session $session */
        $session = $this->get('session');
        $message = 'Data did not steal';

        if ($request->getMethod() == $request::METHOD_POST) {
            /** @var DataStealer $DataStealer */
            $dataStealer = $this->get(DataStealer::class);
            /** @var Creator $creator */
            $creator = $this->get(Creator::class);

            $data = $dataStealer->executing();
            $creator->creating($data);

            $message = 'Steal data was success';
        }
        $session->getFlashBag()->add('message', $message);


        return $this->redirectToRoute('homepage', ['message' => $message]);
    }

    /**
     * @Route("/send-letter", name="send_letter")
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendingLetterAction(Request $request)
    {
        /** @var SendEmail $sendEmailService */
        $sendEmailService = $this->get(SendEmail::class);
        /** @var Session $session */
        $session = $this->get('session');
        $message = 'Email did not send';

        $form = $this->createForm(LetterType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $sendEmailService->sendLetter($form->get('email')->getData());
            $message = 'Email was send';
        }

        $session->getFlashBag()->add('message', $message);

        return $this->redirectToRoute('homepage', [
                'letterForm' => $form,
                'message' => $message
        ]);
    }

    /**
     * @Route("/create-xml", name="create_xml", methods="POST")
     * @param Request $request
     * @return RedirectResponse
     */
    public function createXMLAction(Request $request)
    {
        /** @var CreateFileXML $creatorXML */
        $creatorXML = $this->get(CreateFileXML::class);
        /** @var Session $session */
        $session = $this->get('session');
        $message = 'XML did not create';

        if ($request->getMethod() == $request::METHOD_POST) {
            $creatorXML->execute();
            $message = 'XML Was create';
        }

        $session->getFlashBag()->add('message', $message);

        return $this->redirectToRoute('homepage');
    }
}
