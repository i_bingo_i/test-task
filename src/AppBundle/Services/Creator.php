<?php

namespace AppBundle\Services;

use AppBundle\Entity\WorldMarkets;
use AppBundle\Repository\WorldMarkets as WorldMarketsRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Creator
{
    /** @var ObjectManager */
    private $om;
    /** @var WorldMarketsRepository */
    private $worldMarketsRepository;
    /** @var ContainerInterface */
    private $container;
    /** @var Logger */
    private $logger;

    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->om = $objectManager;
        $this->container = $serviceContainer;
        $this->worldMarketsRepository = $this->om->getRepository('AppBundle:WorldMarkets');
        $this->logger = $this->container->get('monolog.logger.channel1');
    }

    /**
     * @param array $data
     */
    public function creating(array $data)
    {
        $this->logger->info("Start saving data to database");

        if (!empty($data)) {
            foreach ($data as $oneCompany) {
                if ($existCompany = $this->existCompany($oneCompany)) {
                    $this->update($existCompany, $oneCompany);
                } else {
                    $this->create($oneCompany);
                }
            }

            $this->om->flush();
        }

        $this->logger->info("Finish saving data to database");
    }

    /**
     * @param array $oneCompany
     */
    private function create(array $oneCompany)
    {
        $newCompany = new WorldMarkets();
        $newCompany->setCompanySymbol($oneCompany['companySymbol']);
        $newCompany->setCompanyName($oneCompany['companyName']);
        $newCompany->setPrice($oneCompany['price']);
        $newCompany->setPercent($oneCompany['percent']);
        $newCompany->setHigh($oneCompany['high']);
        $newCompany->setLow($oneCompany['low']);
        $newCompany->setOpen($oneCompany['open']);

        $this->om->persist($newCompany);
    }

    /**
     * @param WorldMarkets $existCompany
     * @param $oneCompany
     */
    private function update(WorldMarkets $existCompany, $oneCompany)
    {
        $existCompany->setCompanySymbol($oneCompany['companySymbol']);
        $existCompany->setCompanyName($oneCompany['companyName']);
        $existCompany->setPrice($oneCompany['price']);
        $existCompany->setPercent($oneCompany['percent']);
        $existCompany->setHigh($oneCompany['high']);
        $existCompany->setLow($oneCompany['low']);
        $existCompany->setOpen($oneCompany['open']);

        $this->om->persist($existCompany);
    }

    /**
     * @param array $oneCompany
     * @return WorldMarkets|null|object
     */
    private function existCompany(array $oneCompany)
    {
        return $this->worldMarketsRepository->findOneBy(['companySymbol' => $oneCompany['companySymbol']]);
    }


}