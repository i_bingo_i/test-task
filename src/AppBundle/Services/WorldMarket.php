<?php

namespace AppBundle\Services;

use AppBundle\Entity\WorldMarkets;
use AppBundle\Repository\WorldMarkets as WorldMarketsRepository;
use Doctrine\Common\Persistence\ObjectManager;

class WorldMarket
{
    /** @var ObjectManager */
    private $om;
    /** @var WorldMarketsRepository */
    private $worldMarketsRepository;

    /**
     * WorldMarket constructor.
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->om = $objectManager;
        $this->worldMarketsRepository = $this->om->getRepository('AppBundle:WorldMarkets');
    }

    /**
     * @return array
     */
    public function getDataForRender()
    {
        $allWorldMarketData = $this->worldMarketsRepository->findAll();

        return $this->prepareData($allWorldMarketData);
    }

    /**
     * @param array $allWorldMarketData
     * @return array
     */
    private function prepareData(array $allWorldMarketData)
    {
        $result = [];
        if (!empty($allWorldMarketData)) {
            /** @var WorldMarkets $market */
            foreach ($allWorldMarketData as $market) {
                $result['names'][] = $market->getCompanySymbol();
                $result['percents'][] = $market->getPercent();
            }
        }

        return $result;
    }
}
