<?php

namespace AppBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;
use Swift_Attachment;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SendEmail
{
    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var \AppBundle\Repository\WorldMarkets */
    private $worldMarketsRepository;
    /** @var CreateFileXML */
    private $createXMLService;
    /** @var object|\Twig\Environment  */
    private $twig;
    /** @var object|\Swift_Mailer  */
    private $mailer;
    /** @var object|\Symfony\Bridge\Monolog\Logger  */
    private $logger;

    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->om = $objectManager;
        $this->container = $serviceContainer;
        $this->worldMarketsRepository = $this->om->getRepository('AppBundle:WorldMarkets');
        $this->createXMLService = $this->container->get(CreateFileXML::class);
        $this->twig = $this->container->get('twig');
        $this->mailer = $this->container->get('mailer');
        $this->logger = $this->container->get('monolog.logger.channel1');
    }

    /**
     * @param string $email
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function sendLetter($email)
    {
        $theBestMarketData = $this->worldMarketsRepository->findBy([], ['percent' => 'DESC'], 5);

        if (!$this->createXMLService->isExistFile()) {
            $this->createXMLService->execute();
        }

        $message = (new \Swift_Message('Hello Email'))
            ->addFrom('alex.babich@idapgroup.com')
            ->addTo($email)
            ->setBody(
                $this->twig->render('default/email-message.html.twig', ['data' => $theBestMarketData]),
                'text/html'
            )
            ->attach(Swift_Attachment::fromPath($this->createXMLService->getFileName()));

        $this->logger->info("Sending email on {$email} address");
        $this->mailer->send($message);
    }
}