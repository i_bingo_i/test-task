<?php

namespace AppBundle\Services;

use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;

class DataStealer
{
    /** @var array */
    private $companies;
    /** @var ContainerInterface */
    private $container;
    /** @var Logger */
    private $logger;

    /**
     * DataStealer constructor.
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ContainerInterface $serviceContainer)
    {
        $this->container = $serviceContainer;
        $this->logger = $this->container->get('monolog.logger.channel1');
    }

    /**
     * @return mixed
     */
    public function executing()
    {
        $this->logger->info("Start stealing data");

        $this->prepareCommonInformation();
        $this->prepareInformationAboutCompany();

        $this->logger->info("Finish stealing data");

        return $this->companies;
    }

    /**
     * Steal fields CompanySymbol, CompanyName, Price, Change
     * @return bool
     */
    private function prepareCommonInformation()
    {
        $index = 0;
        $financePage = file_get_contents(
            'https://finance.yahoo.com/screener/unsaved/76f2478f-ebf9-4ee5-bf36-49f1a1603a9b'
        );

        $crawler = new Crawler($financePage);

        $crawler->filter('div[class="Ovx(s)"] tbody tr td')->each(function (Crawler $node, $i) use (&$index) {
            if ($node->attr('aria-label') == 'Symbol') {
                $index++;
                $this->companies[$index]['companySymbol'] = $node->text();
            }

            if ($node->attr('aria-label') == 'Name') {
                $this->companies[$index]['companyName'] = $node->text();
            }

            if ($node->attr('aria-label') == 'Price (Intraday)') {
                $this->companies[$index]['price'] = $node->text();
            }

            if ($node->attr('aria-label') == '% Change') {
                $this->companies[$index]['percent'] = trim($node->text(), '%');
            }
        });

        return count($this->companies) ? true : false;
    }

    /**
     * Steal fields Open, High, Low
     */
    private function prepareInformationAboutCompany()
    {
        foreach ($this->companies as $kye => $company) {
            /** Field Open */
            $oneCompanyPage = file_get_contents(
                "https://finance.yahoo.com/quote/{$company['companySymbol']}?p={$company['companySymbol']}&guccounter=1"
            );
            $oneCompanyPageCrawler = new Crawler($oneCompanyPage);

            $this->companies[$kye]['open'] = $oneCompanyPageCrawler
                ->filter('tbody[data-reactid="36"] span[data-reactid="46"]')->text();
            /** end */

            /** Fields High and Low */
            $oneCompanyPageStatistics = file_get_contents(
                "https://finance.yahoo.com/quote/{$company['companySymbol']}/key-statistics?p={$company['companySymbol']}"
            );

            $oneCompanyPageStatisticsCrawler = new Crawler($oneCompanyPageStatistics);

            $this->companies[$kye]['high'] = $oneCompanyPageStatisticsCrawler
                ->filter('section div[class="Fl(end) W(50%) smartphone_W(100%)"] table tr')
                ->eq(3)->filter('td[class="Fz(s) Fw(500) Ta(end)"]')->text();
            $this->companies[$kye]['low'] = $oneCompanyPageStatisticsCrawler
                ->filter('section div[class="Fl(end) W(50%) smartphone_W(100%)"] table tr')
                ->eq(4)->filter('td[class="Fz(s) Fw(500) Ta(end)"]')->text();
        }
    }
}
