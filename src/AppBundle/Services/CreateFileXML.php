<?php

namespace AppBundle\Services;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AppBundle\Repository\WorldMarkets as WorldMarketsRepository;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class CreateFileXML
{
    /** @var ObjectManager */
    private $om;
    /** @var ContainerInterface */
    private $container;
    /** @var WorldMarketsRepository */
    private $worldMarketsRepository;
    /** @var string */
    private $fileName;
    /** @var Filesystem */
    private $fileSystem;
    /** @var KernelInterface */
    private $kernel;
    /** @var Logger  */
    private $logger;

    /**
     * CreateFileXML constructor.
     * @param ObjectManager $objectManager
     * @param ContainerInterface $serviceContainer
     */
    public function __construct(ObjectManager $objectManager, ContainerInterface $serviceContainer)
    {
        $this->om = $objectManager;
        $this->container = $serviceContainer;
        $this->worldMarketsRepository = $this->om->getRepository('AppBundle:WorldMarkets');
        $this->kernel = $this->container->get('kernel');
        $this->fileSystem = new Filesystem();
        $this->logger = $this->container->get('monolog.logger.channel1');

        $this->fileName = $this->kernel->getRootDir() . '/../web/uploads/MarketData.xml';
    }

    /**
     * @return bool
     */
    public function execute()
    {
        $fiveTheBestResults = $this->worldMarketsRepository->findAll();
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $xml = $serializer->serialize($fiveTheBestResults, 'xml');

        $this->createIfDoesNotExist();

        $this->logger->info("Start generating xml file");

        $this->createNewFile($xml);

        $this->logger->info("Finish generating xml file");

        return true;
    }

    /**
     * @return bool
     */
    public function isExistFile()
    {
        return $this->fileSystem->exists($this->fileName);
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $xml
     */
    private function createNewFile($xml)
    {
        if ($this->fileSystem->exists($this->fileName)) {
            $this->fileSystem->remove($this->fileName);
        }

        if (!$this->fileSystem->exists($this->fileName)) {
            $this->fileSystem->touch($this->fileName);
            $this->fileSystem->appendToFile($this->fileName, $xml);
        }
    }

    private function createIfDoesNotExist()
    {
        $uploadDir = $this->kernel->getRootDir() . '/../web/uploads';
        if (!$this->fileSystem->exists($uploadDir)) {
            $this->logger->info("Create upload directory");

            $this->fileSystem->mkdir($uploadDir);
            $this->fileSystem->chmod($uploadDir, 0775, 0000, true);
        }
    }
}
