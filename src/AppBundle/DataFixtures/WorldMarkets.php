<?php

namespace AppBundle\DataFixtures;

use AppBundle\Entity\WorldMarkets as WorldMarketsEntity;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class WorldMarkets extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $market1 = new WorldMarketsEntity();
        $market1->setCompanySymbol('BCE');
        $market1->setCompanyName('BCE Inc.');
        $market1->setPercent(-0.07);
        $market1->setPrice(42.52);
        $market1->setOpen(42.57);
        $market1->setHigh(49.06);
        $market1->setLow(40.19);

        $manager->persist($market1);

        $market2 = new WorldMarketsEntity();
        $market2->setCompanySymbol('AMX');
        $market2->setCompanyName('América Móvil, S.A.B. de C.V.');
        $market2->setPercent(-0.96);
        $market2->setPrice(17.57);
        $market2->setOpen(17.61);
        $market2->setHigh(19.91);
        $market2->setLow(14.85);

        $manager->persist($market2);

        $manager->flush();
    }
}