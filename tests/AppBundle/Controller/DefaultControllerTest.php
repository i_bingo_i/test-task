<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{

    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    public function testStealData()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler
            ->filter('.steal-data')
            ->form();

        $client->submit($form);

        $this->assertTrue(
            $client->getResponse()->isRedirect()
        );

        $crawler = $client->followRedirect();

        $this->assertGreaterThan(
            0,
            $crawler->filter('.messages:contains("Steal data was success")')->count()
        );
    }

    public function testCreateXML()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler
            ->filter('.xml-create')
            ->form();

        $client->submit($form);

        $this->assertTrue(
            $client->getResponse()->isRedirect()
        );

        $crawler = $client->followRedirect();

        $this->assertGreaterThan(
            0,
            $crawler->filter('.messages:contains("XML Was create")')->count()
        );
    }

    public function testSendEmail()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler
            ->filter('.send-letter')
            ->form();

        $form['app_bundle_letter_type[email]'] = 'ibabuch88i@gmail.com';
        $client->submit($form);

        $this->assertTrue(
            $client->getResponse()->isRedirect()
        );

        $crawler = $client->followRedirect();

        $this->assertGreaterThan(
            0,
            $crawler->filter('.messages:contains("Email was send")')->count()
        );
    }
}
