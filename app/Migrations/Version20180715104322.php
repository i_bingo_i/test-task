<?php

namespace App\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180715104322 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('ALTER TABLE world_markets ADD COLUMN date_update DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TEMPORARY TABLE __temp__world_markets AS SELECT id, company_symbol, company_name, price, percent, open, high, low, date_create FROM world_markets');
        $this->addSql('DROP TABLE world_markets');
        $this->addSql('CREATE TABLE world_markets (id INTEGER NOT NULL, company_symbol VARCHAR(255) DEFAULT NULL, company_name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, percent DOUBLE PRECISION NOT NULL, open DOUBLE PRECISION DEFAULT NULL, high DOUBLE PRECISION DEFAULT NULL, low DOUBLE PRECISION DEFAULT NULL, date_create DATETIME NOT NULL, PRIMARY KEY(id))');
        $this->addSql('INSERT INTO world_markets (id, company_symbol, company_name, price, percent, open, high, low, date_create) SELECT id, company_symbol, company_name, price, percent, open, high, low, date_create FROM __temp__world_markets');
        $this->addSql('DROP TABLE __temp__world_markets');
    }
}
